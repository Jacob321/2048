﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace _2048_1
{


    class Program
    {

        public int pause; // zmienna tymczasowa
        public static int board_clear;
        public static int menu, sizeBoard;
        public int button;             //klawisz
        public int historyXcoordinate1, historyYcoordinate1;
        public int historyXcoordinate2, historyYcoordinate2;
        public static string exitt;
        public static int[,] board = new int[19, 19];
        public static int coordinateXnumber1, coordinateYnumber1, coordinateXnumber2, coordinateYnumber2;
        //obramowanie
        public static char lu = '-';  // lewe górne                //lg
        public static char ld = '-';  // lewe dolne              //ld
        public static char ru = '-';  // prawe górne             //pg
        public static char rd = '-';  // prawe dolne             //pd
        public static char rld = '-'; // prawo lewo dol          //pld
        public static char rlu = '-'; // prawo lewo gora         //plg
        public static char udr = '|'; // gora dol prawo          //gdp
        public static char udl = '|'; // gora dol lewo           //gdl
        public static char udrl = '|';// gora dol prawo lewo     //gdpl
        public static char hor = '-'; // poziome (horizon)        //poz
        public static char ver = '|';// pionowe (vertical)       //pion
                                   //sterowanie

        public static ConsoleKey key1;
        /*         int sg = 296;   // strzalka do gory
                int sd = 304;   // strzalka na dol
                int sp = 301;   // strzalka w prawo
                int sl = 299;   // strzalka w lewo
                int esc = 27;
                int w = 119;    // klawisz "w"
                int a = 97;     // klawisz "a"
                int s = 115;    // klawisz "s"
                int d = 100;    // klawisz "d"
        */
        public static int emptyy = 0; // pole puste
        public static int rand1 = 2;      // pierwsza losowa liczba
        public static int rand2 = 4;      // druga losowa liczba
        public static int randNumber = 0;

        public static int check_y = 1;                                    // sprawdzenie po zmiennej y
        public static int check_y_next, check_y_next2, check_y_next4;     // sprawdzenie kolejnej wartosci zmiennej y

        public static int check_x = 1;                                    // sprawdzenie po zmiennej x
        public static int check_x_next, check_x_next2, check_x_next4;     // sprawdzenie kolejnej wartosci zmiennej x

        public static int check_x_priev, check_x_priev2, check_x_priev4;      // sprawdzenie poprzedniej wartosci zmiennej x
        public static int check_y_priev, check_y_priev2, check_y_priev4;      // sprawdzenie poprzedniej wartosci zmiennej y

        public static int check_move = 0;

        public static ConsoleKeyInfo consoleKeyInfo;




        public static int menuu(int _menu)
    {
            
            switch (_menu)
            {
                case 1:
                    Console.Clear();
                    Console.WriteLine("----------------------------------------");
                    Console.WriteLine("Wybierz rozmiar planszy:");
                    Console.WriteLine("----------------------------------------");
                    Console.WriteLine("1. 4 x 4 (standardowy rozmiar)");
                    Console.WriteLine("2. 6 x 6 (niestandardowy rozmiar)    coming soon ");
                    Console.WriteLine("3. 8 x 8 (niestandardowy rozmiar)    coming soon ");
                    Console.WriteLine("----------------------------------------");
                    sizeBoard = Int32.Parse(Console.ReadLine());
                    break;
                case 2:
                    Console.Clear();
                    Console.WriteLine("----------------------------------------");
                    Console.WriteLine(" \tSterowanie: ");
                    Console.WriteLine("----------------------------------------");
                    Console.WriteLine("\tKlawiszami W, A, S, D ");
                    Console.WriteLine("\tStrzalkami \n\n");
                    Console.WriteLine("----------------------------------------");
                    Console.WriteLine("Aby wyjsc wcisnij dowolny klawisz");
                    Console.WriteLine("----------------------------------------");
                    Console.ReadLine();
                    break;
                case 3:
                    Console.Clear();
                    Console.WriteLine("----------------------------------------");
                    Console.WriteLine("\tInformacje ");
                    Console.WriteLine("----------------------------------------");
                    Console.WriteLine("\n\tCreated by Jakub Piskorowski ");
                    Console.WriteLine("\tEmail: jacobmasterr@gmail.com \n\n");
                    Console.WriteLine("----------------------------------------");
                    Console.WriteLine("Aby wyjsc wcisnij dowolny klawisz ");
                    Console.WriteLine("----------------------------------------");
                    Console.ReadLine();
                    break;
                case 4:
                    Console.Clear();
                    Console.WriteLine("----------------------------------------");
                    Console.WriteLine("Na pewno chcesz wyjsc? ( y / n ) ");
                    Console.WriteLine("----------------------------------------");
                    exitt = Console.ReadLine();
                    if (exitt == "y")
                    {
                        Environment.Exit(0);        // zamknięcie programu
                    }
                    break;
            }
            return _menu; 
    }


        public static void createBoard(int _sizeBoard)
        {
            // nadajemy wartoœæ 0000 (pole puste) tablicy "pole"

            for (int i = 0; i < _sizeBoard; i++)
            {
                for (int j = 0; j < _sizeBoard; j++)
                {
                    board[i, j] = emptyy;
                }
            }
        }


        public static int randValue()
        {
            Random rand = new Random();
            int a = rand.Next(1, 100);
            if (a < 85)
            {
                randNumber = rand1;
                return randNumber;
            }
            else if (a >= 85)
            {
                randNumber = rand2;
                return randNumber;
            }
            else
        return randNumber;
        }


        public static void randCoordinates(int _sizeBoard)
        {
            do
            {
                do
                {
                Random rand = new Random();
                coordinateXnumber1 = rand.Next(0, _sizeBoard);
                } while (coordinateXnumber1 % 2 != 1);

                do
                {
                Random rand = new Random();
                coordinateYnumber1 = rand.Next(0, _sizeBoard);
                } while (coordinateYnumber1 % 2 != 1); 

            } while (board[coordinateXnumber1, coordinateYnumber1] != emptyy);
            board[coordinateXnumber1, coordinateYnumber1] = randNumber;
        }


        public static void checkKey (int _sizeBoard, ConsoleKey _key1)
        {
            _sizeBoard++;

            check_move = 0;
            if (_key1 == ConsoleKey.W || _key1 == ConsoleKey.UpArrow)
            {
                    for (check_x = 1; check_x < _sizeBoard; check_x += 2)
                    {
                        for (check_y = 1; check_y < _sizeBoard; check_y += 2)
                        {
                            check_y_next = check_y + 2;
                            check_y_next2 = check_y_next + 2;
                            check_y_next4 = check_y_next + 4;

                            if (board[check_y, check_x] == board[check_y_next, check_x] && board[check_y_next, check_x] != emptyy)  // dodanie liczb z wiersza (1 i 3)
                            {
                                board[check_y, check_x] += board[check_y_next, check_x];
                                board[check_y_next, check_x] = emptyy;
                                check_move += 1;
                            }
                            if (board[check_y, check_x] == board[check_y_next2, check_x] && board[check_y_next, check_x] == emptyy && board[check_y_next2, check_x] != emptyy)  // dodanie wiersza (1 i 5) jezli 3 jest pusty
                            {
                                board[check_y, check_x] += board[check_y_next2, check_x];
                                board[check_y_next2, check_x] = emptyy;
                                check_move += 1;
                            }
                            if (board[check_y, check_x] == board[check_y_next4, check_x] && board[check_y_next, check_x] == emptyy && board[check_y_next2, check_x] == emptyy && board[check_y_next4, check_x] != emptyy)   // dodanie wiersza (1 i 7) jezli 3 i 5 jest pusty
                            {
                                board[check_y, check_x] += board[check_y_next4, check_x];
                                board[check_y_next4, check_x] = emptyy;
                                check_move += 1;
                            }
                            if (board[check_y, check_x] == emptyy && board[check_y_next, check_x] != emptyy)    // jesli wiersz 1 jest pusty wstaw zawartosc wiersza 3
                            {
                                board[check_y, check_x] = board[check_y_next, check_x];
                                board[check_y_next, check_x] = emptyy;
                                check_move += 1;
                            }
                            if (board[check_y, check_x] == emptyy && board[check_y_next2, check_x] != emptyy)   // jesli wiersz 1 jest pusty wstaw zawartosc wiersza 5
                            {
                                board[check_y, check_x] = board[check_y_next2, check_x];
                                board[check_y_next2, check_x] = emptyy;
                                check_move += 1;
                            }
                            if (board[check_y, check_x] == emptyy && board[check_y_next4, check_x] != emptyy)   // jesli wiersz 1 jest pusty wstaw zawartosc wiersza 7
                            {
                                board[check_y, check_x] = board[check_y_next4, check_x];
                                board[check_y_next4, check_x] = emptyy;
                                check_move += 1;
                            }
                        }
                    }

            }


            if (_key1 == ConsoleKey.A || _key1 == ConsoleKey.LeftArrow)
            {
                    for (check_x = 1; check_x < _sizeBoard; check_x += 2)
                    {
                        for (check_y = 1; check_y < _sizeBoard; check_y += 2)
                        {
                            check_x_next = check_x + 2;
                            check_x_next2 = check_x_next + 2;
                            check_x_next4 = check_x_next + 4;

                            if (board[check_y, check_x] == board[check_y, check_x_next] && board[check_y, check_x_next] != emptyy)  // dodanie liczb z wiersza (1 i 3)
                            {
                                board[check_y, check_x] += board[check_y, check_x_next];
                                board[check_y, check_x_next] = emptyy;
                                check_move += 1;
                            }
                            if (board[check_y, check_x] == board[check_y, check_x_next2] && board[check_y, check_x_next2] == emptyy && board[check_y, check_x_next2] != emptyy) // dodanie wiersza (1 i 5) jezli 3 jest pusty
                            {
                                board[check_y, check_x] += board[check_y, check_x_next2];
                                board[check_y, check_x_next2] = emptyy;
                                check_move += 1;
                            }
                            if (board[check_y, check_x] == board[check_y, check_x_next4] && board[check_y, check_x_next] == emptyy && board[check_y, check_x_next2] == emptyy && board[check_y, check_x_next4] != emptyy)   // dodanie wiersza (1 i 7) jezli 3 i 5 jest pusty
                            {
                                board[check_y, check_x] += board[check_y, check_x_next4];
                                board[check_y, check_x_next4] = emptyy;
                                check_move += 1;
                            }
                            if (board[check_y, check_x] == emptyy && board[check_y, check_x_next] != emptyy)    // jesli wiersz 1 jest pusty wstaw zawartosc wiersza 3
                            {
                                board[check_y, check_x] = board[check_y, check_x_next];
                                board[check_y, check_x_next] = emptyy;
                                check_move += 1;
                            }
                            if (board[check_y, check_x] == emptyy && board[check_y, check_x_next2] != emptyy)   // jesli wiersz 1 jest pusty wstaw zawartosc wiersza 5
                            {
                                board[check_y, check_x] = board[check_y, check_x_next2];
                                board[check_y, check_x_next2] = emptyy;
                                check_move += 1;
                            }
                            if (board[check_y, check_x] == emptyy && board[check_y, check_x_next4] != emptyy)   // jesli wiersz 1 jest pusty wstaw zawartosc wiersza 7
                            {
                                board[check_y, check_x] = board[check_y, check_x_next4];
                                board[check_y, check_x_next4] = emptyy;
                                check_move += 1;
                            }
                        }
                    }
            }


            _sizeBoard-=2;

            if (_key1 == ConsoleKey.D || _key1 == ConsoleKey.RightArrow)
            {

                for (check_x = _sizeBoard; check_x >= 1; check_x -= 2)
                    {
                        for (check_y = _sizeBoard; check_y >= 1; check_y -= 2)
                        {
                            check_x_priev = check_x - 2;
                            check_x_priev2 = check_x_priev - 2;
                            check_x_priev4 = check_x_priev - 4;
                        if (check_x_priev4 > 0)
                        {
                            if (board[check_y, check_x] == board[check_y, check_x_priev4] && board[check_y, check_x_priev] == emptyy && board[check_y, check_x_priev2] == emptyy && board[check_y, check_x_priev4] != emptyy)   // dodanie wiersza (1 i 7) jezli 3 i 5 jest pusty
                            {
                                board[check_y, check_x] += board[check_y, check_x_priev4];
                                board[check_y, check_x_priev4] = emptyy;
                                check_move += 1;
                            }
                        }
                        if (check_x_priev4 > 0)
                        {
                            if (board[check_y, check_x] == emptyy && board[check_y, check_x_priev4] != emptyy)  // jesli wiersz 1 jest pusty wstaw zawartosc wiersza 7
                            {
                                board[check_y, check_x] = board[check_y, check_x_priev4];
                                board[check_y, check_x_priev4] = emptyy;
                                check_move += 1;
                            }
                        }
                        if (check_x_priev2 > 0)
                        {
                            if (board[check_y, check_x] == board[check_y, check_x_priev2] && board[check_y, check_x_priev2] == emptyy && board[check_y, check_x_priev2] != emptyy)  // dodanie wiersza (1 i 5) jezli 3 jest pusty
                            {
                                board[check_y, check_x] += board[check_y, check_x_priev2];
                                board[check_y, check_x_priev2] = emptyy;
                                check_move += 1;
                            }
                        }
                        if (check_x_priev2 > 0)
                        {
                            if (board[check_y, check_x] == emptyy && board[check_y, check_x_priev2] != emptyy)  // jesli wiersz 1 jest pusty wstaw zawartosc wiersza 5
                            {
                                board[check_y, check_x] = board[check_y, check_x_priev2];
                                board[check_y, check_x_priev2] = emptyy;
                                check_move += 1;
                            }
                        }
                        if (check_x_priev > 0)
                        {
                            if (board[check_y, check_x] == board[check_y, check_x_priev] && board[check_y, check_x_priev] != emptyy)    // dodanie liczb z wiersza (1 i 3)
                            {
                                board[check_y, check_x] += board[check_y, check_x_priev];
                                board[check_y, check_x_priev] = emptyy;
                                check_move += 1;
                            }
                        }
                        if (check_x_priev > 0)
                        {
                            if (board[check_y, check_x] == emptyy && board[check_y, check_x_priev] != emptyy)   // jesli wiersz 1 jest pusty wstaw zawartosc wiersza 3
                            {
                                board[check_y, check_x] = board[check_y, check_x_priev];
                                board[check_y, check_x_priev] = emptyy;
                                check_move += 1;
                            }
                        }


                        }
                    }
            }


            

            if (_key1 == ConsoleKey.S || _key1 == ConsoleKey.DownArrow)
            {
                    for (check_x = _sizeBoard; check_x >= 1; check_x -= 2)
                    {
                        for (check_y = _sizeBoard; check_y >= 1; check_y -= 2)
                        {
                            check_y_priev = check_y - 2;
                            check_y_priev2 = check_y_priev - 2;
                            check_y_priev4 = check_y_priev - 4;

                        if (check_y_priev > 0)
                        {
                            if (board[check_y, check_x] == board[check_y_priev, check_x] && board[check_y_priev, check_x] != emptyy)    // dodanie liczb z wiersza (1 i 3)
                            {
                                board[check_y, check_x] += board[check_y_priev, check_x];
                                board[check_y_priev, check_x] = emptyy;
                                check_move += 1;
                            }
                        }
                        if (check_y_priev2 > 0)
                        {
                            if (board[check_y, check_x] == board[check_y_priev2, check_x] && board[check_y_priev, check_x] == emptyy && board[check_y_priev2, check_x] != emptyy)   // dodanie wiersza (1 i 5) jezli 3 jest pusty
                            {
                                board[check_y, check_x] += board[check_y_priev2, check_x];
                                board[check_y_priev2, check_x] = emptyy;
                                check_move += 1;
                            }
                        }
                        if (check_y_priev4 > 0)
                        {
                            if (board[check_y, check_x] == board[check_y_priev4, check_x] && board[check_y_priev, check_x] == emptyy && board[check_y_priev2, check_x] == emptyy && board[check_y_priev4, check_x] != emptyy)   // dodanie wiersza (1 i 7) jezli 3 i 5 jest pusty
                            {
                                board[check_y, check_x] += board[check_y_priev4, check_x];
                                board[check_y_priev4, check_x] = emptyy;
                                check_move += 1;
                            }
                        }
                        if (check_y_priev > 0)
                        {
                            if (board[check_y, check_x] == emptyy && board[check_y_priev, check_x] != emptyy)   // jesli wiersz 1 jest pusty wstaw zawartosc wiersza 3
                            {
                                board[check_y, check_x] = board[check_y_priev, check_x];
                                board[check_y_priev, check_x] = emptyy;
                                check_move += 1;
                            }
                        }
                        if (check_y_priev2 > 0)
                        {
                            if (board[check_y, check_x] == emptyy && board[check_y_priev2, check_x] != emptyy)  // jesli wiersz 1 jest pusty wstaw zawartosc wiersza 5
                            {
                                board[check_y, check_x] = board[check_y_priev2, check_x];
                                board[check_y_priev2, check_x] = emptyy;
                                check_move += 1;
                            }
                        }

                        }
                    }
            }
        }


        public static void drawBoard(int _sizeBoard)
        {
            // gora ramki
            Console.Write(lu);
            for (int i = 1; i < _sizeBoard; i++)
            {
                if (i % 2 == 0)
                {
                    Console.Write(rld);
                }
                else
                {
                    Console.Write(hor +""+ hor +""+ hor +""+ hor);
                }
            }
            Console.Write ( ru );
            
            // Lewa, Prawa, Œrodek ramki
            for (int i = 1; i < _sizeBoard; i++)
            {
                if (i % 2 == 0)
                {
                    Console.Write("\n" + udr);    // lewa ramka
                }
                else
                {
                    Console.Write("\n" + ver);   // lewa ramka
                }
                for (int j = 1; j < _sizeBoard; j++)
                {
                    if (i % 2 == 1 && j % 2 == 1 && board[i, j] == emptyy)
                    {
                        Console.Write("    ");     // puste pole
                    }
                    if (board[i, j] == randNumber)
                    {
                        Console.Write("   " + randNumber);     // losowa liczba
                    }
                    /*	if (board[i][j] == rand2)
                        {
                            cout << "   " << rand2;		// losowa liczba
                        }
                    */
                    if (i % 2 == 1 && j % 2 == 1 && board[i, j] != emptyy && board[i, j] != randNumber /*&& board[i][j] != rand2 */)
                    {
                        if (board[i, j] < 10)
                        {
                            Console.Write("   " + board[i, j]);         // wyswietlenie zsumowanych liczb do 10
                        }
                        if (board[i, j] < 100 && board[i, j] > 9)
                        {
                            Console.Write("  " + board[i, j]);          // wyswietlenie zsumowanych liczb do 100
                        }
                        if (board[i, j] < 1000 && board[i, j] > 99)
                        {
                            Console.Write(" " + board[i, j]);           // wyswietlenie zsumowanych liczb do 1000
                        }
                        if (board[i, j] < 10000 && board[i, j] > 999)
                        {
                            Console.Write("", board[i, j]);            // wyswietlenie zsumowanych liczb do 2048
                        }
                    }
                    if (i % 2 == 0 && j % 2 == 0)
                    {
                        Console.Write(udrl);       // gora dol prawo lewo
                    }
                    if (i % 2 == 0 && j % 2 == 1)
                    {
                        Console.Write(hor +""+ hor +""+ hor +""+ hor);   // pozioma
                    }
                    if (j % 2 == 0 && i % 2 == 1)
                    {
                        Console.Write(ver);       // pionowa
                    }
                }
                if (i % 2 == 0)
                {
                    Console.Write(udl);    // lewa ramka
                }
                else
                {
                    Console.Write(ver);   // lewa ramka
                }
            }
            Console.Write("\n");
            // dól ramki
            Console.Write(ld);
            for (int i = 1; i < _sizeBoard; i++)
            {
                if (i % 2 == 0)
                {
                    Console.Write(rlu);
                }
                else
                {
                    Console.Write(hor +""+ hor +""+ hor +""+ hor);
                }
            }
            Console.Write(rd);
        }


        public static void checkclear()
        {
            board_clear = 0;            // Sprawdzenie czy sa puste pola
            for (int i = 1; i < 9; i += 2)
            {
                for (int j = 1; j < 9; j += 2)
                {
                    if (board[i, j] == emptyy)
                    {
                        board_clear += 1;
                    }
                }
            }
        }

       static void Main(string[] args)
        {
            do
            {
                
                Console.WriteLine("----------------------------------------");
                Console.WriteLine("\tMENU ");
                Console.WriteLine("----------------------------------------");
                Console.WriteLine("\t1. Start");
                Console.WriteLine("\t2. Sterowanie");
                Console.WriteLine("\t3. Informacje");
                Console.WriteLine("\t4. Wyjscie");
                Console.WriteLine("----------------------------------------");
                menu = Int32.Parse(Console.ReadLine());
                
                menuu(menu);

                if (menu == 1)
                {

                    if (sizeBoard == 1)
                    {
                        sizeBoard = 8;
                    }
                    else if (sizeBoard == 2)
                    {
                        sizeBoard = 8;     // 12
                    }
                    else if (sizeBoard == 3)
                    {
                        sizeBoard = 8;     // 16
                    }

                    createBoard(sizeBoard);     // wypełnienie tablicy zerami

                    randValue();    // losuje wartość 2 lub 4
                    randCoordinates(sizeBoard);     // losowanie wspolrzednych nowej liczby


                    ConsoleKey key1;

                    consoleKeyInfo = Console.ReadKey();
                    key1 = consoleKeyInfo.Key;

                    do
                    {
                        Console.Clear();

                        randValue();    // losuje wartość 2 lub 4

                        checkKey(sizeBoard, key1);        // sprawdzanie klawisza i wykonanie ruchu 

                        checkclear();   // sprawdzenie czy sa puste pola 

                        if (check_move > 0)         // Jesli wykonano jakis ruch
                        {
                            randCoordinates(sizeBoard);     // losowanie wspolrzednych nowej liczby
                        }

                        drawBoard(sizeBoard);       // rysowanie pola

                        if (board_clear == 0 && check_move == 0)    // Jesli nie ma wolnych pol to koniec gry
                        {
                            Console.WriteLine("\n\n\n KONIEC \n\n");
                            do
                            {
                                consoleKeyInfo = Console.ReadKey();
                                key1 = consoleKeyInfo.Key;

                                Console.WriteLine("\n\nAby wyjsc wcisnij klawisz \"Esc\" ");
                                Environment.Exit(0);        // zamknięcie programu
                            } while (key1 == ConsoleKey.Escape);
                            //return 0;

                        }

                        consoleKeyInfo = Console.ReadKey();
                        key1 = consoleKeyInfo.Key;

                    } while (key1 == ConsoleKey.W || key1 == ConsoleKey.UpArrow || key1 == ConsoleKey.A || key1 == ConsoleKey.LeftArrow || key1 == ConsoleKey.D || key1 == ConsoleKey.RightArrow || key1 == ConsoleKey.S || key1 == ConsoleKey.DownArrow);
                }

                Console.ReadLine();

                Console.Clear();
                
            } while (menu == 1 || menu == 2 || menu == 3 || menu == 4);

        }

       
    }
}